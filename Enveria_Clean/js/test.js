//Ocultar Loading
/*$(window).load(function () {

    //$("#Ingreso_mensual").mask("999?,999,999", { placeholder : "" });
    //$("#load").removeClass("loader");
    $("#load").fadeOut("slow");

});*/


$(window).bind('load', function() {
    //after everything has loaded
	$("#load").fadeOut("slow");
});


///////////////////////////////////////////////////////////////Formulario 2

$("#tel_trabajo").keydown(function (e) {
    restringirNumeros(e);
});


//Ingreso Mensual
$("#Ingreso_mensual").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Ingreso_mensual").keydown(function (e) {
    restringirNumeros(e);
});


//Promedio_Gasto
$("#Promedio_Gasto").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Promedio_Gasto").keydown(function (e) {
    restringirNumeros(e);
});


//Luz Agua Gas etc.
$("#Luz_etc").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Luz_etc").keydown(function (e) {
    restringirNumeros(e);
});




//Promedio Tarjetas
$("#Promedio_pago_Tarjetas").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Promedio_pago_Tarjetas").keydown(function (e) {
    restringirNumeros(e);
});



//Entretenimieno
$("#Entretenimiento").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Entretenimiento").keydown(function (e) {
    restringirNumeros(e);
});



//Gastos_Transporte
$("#Gastos_Transporte").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});


$("#Gastos_Transporte").keydown(function (e) {
    restringirNumeros(e);
});


//Otros Gastos
$("#Otros_Gastos").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#Otros_Gastos").keydown(function (e) {
    restringirNumeros(e);
});


//////////////////////////////////////////////////////////Formulario 1

//Al escribir codigo postal
$("#codigo_Postal").blur(function () {
    var codigoPostal = $(this).val();
    cargarColonias(codigoPostal);
});

$("#telefono_1").keydown(function (e) {
    restringirNumeros(e);
});


$("#TelRef1").keydown(function (e) {
    restringirNumeros(e);
});

$("#TelRef2").keydown(function (e) {
    restringirNumeros(e);
});


$('#RFC').click(function () {
    this.focus();
});


$("#RFC").blur(function () {
    var rfcString = $(this).val().trim().toUpperCase();
    var validado = rfcValido(rfcString);
    console.log("input: " + validado);
    if (validado) {
        //alert("correcto");
    } else {
        alert("RFC invalido, asegurate de ingresar tu homoclave");
        $(this).val("");
    }

});

//Al seleccionar estado
var selectEstado = "Null";

//Al seleccionar municipio
var selectMunicipio = "Null";



//Cargar colonias con Ajax
function cargarColonias(codigoPostal) {
    //Validar que se haya seleccionado estado y municipio
    /*if(selectEstado == "Null" || selectMunicipio == "Null" || selectEstado.toLocaleLowerCase() == "Ciudad o Estado*".toLocaleLowerCase() || selectMunicipio.toLocaleLowerCase() == "Delegación ó Municipio*".toLocaleLowerCase()){
        alert("Primero debe seleccionar un Estado y Municipio");
        clearCP();
        return;
    }
    else{*/
    showDialog();
    var url = "https://api-codigos-postales.herokuapp.com/v2/codigo_postal/" + codigoPostal; // El script a dÃ³nde se realizarÃ¡ la peticiÃ³n.
    $.ajax({
        type: "GET",
        url: url,
        dataType: "JSON",
        //data: $("#login-form").serialize(), // Adjuntar los campos del formulario enviado.
        success: function (data) {
            //getSelectEstado();
            //getSelectMunicipio();
            var municipioCDP = data.municipio;
            var estadoCDP = data.estado;
            $("#ciudad").val(estadoCDP);
            $("#delegacion").val(municipioCDP);

            //Capturar CP invalido
            if (estadoCDP == "" && municipioCDP == "") {
                clearCP();
                alert("Código postal invalido");
                hideDialog();
                return;
            }

            //Validar codigo postal correspondiente
            //if(selectEstado.toLowerCase() == estadoCDP.toLowerCase()){

            //if(selectMunicipio.toLowerCase() == municipioCDP.toLowerCase()){
            $('#colonia').html("");
            //Rellena el select
            $.each(data.colonias, function (i, colonia) {
                console.log(colonia);
                $('#colonia').append($('<option>', {
                    value: colonia,
                    text: colonia
                }));
            });
            console.log(data);
            //}else alert("El código postal "+codigoPostal+" no coincide con el municipio "+selectMunicipio);
            //}else alert("El código postal "+codigoPostal+" no coincide con el estado "+selectEstado);

            hideDialog();
        }
    });
}


function getSelectEstado() {
    var combo = document.getElementById("ciudad");
    selectEstado = combo.options[combo.selectedIndex].text;
    console.log("Se selecciono " + selectEstado);
}

function getSelectMunicipio() {
    var combo2 = document.getElementById("delegacion");
    selectMunicipio = combo2.options[combo2.selectedIndex].text;
    console.log("Se selecciono " + selectMunicipio);
}

function clearCP() {
    $("#codigo_Postal").val("");
}

/////////////////////////////////////////////////////Formulario 3

//Ingreso Mensual
$("#prestamo").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
    restringirNumeros(e);
});

$("#prestamo").keydown(function (e) {
    restringirNumeros(e);
});


$("#Numero_cuenta").keydown(function (e) {
    restringirNumeros(e);
});




$("#CLABE").keydown(function (e) {
    restringirNumeros(e);
});




/////////////////////////////////////////////////////Formulario 4

$("#file0").change(function(){ validateFileSize(this.id); });
$("#file1").change(function(){ validateFileSize(this.id); });
$("#file2").change(function(){ validateFileSize(this.id); });
$("#file3").change(function(){ validateFileSize(this.id); });
$("#file4").change(function(){ validateFileSize(this.id); });
$("#file5").change(function(){ validateFileSize(this.id); });
$("#file6").change(function(){ validateFileSize(this.id); });
$("#file7").change(function(){ validateFileSize(this.id); });
$("#file8").change(function(){ validateFileSize(this.id); });
$("#file9").change(function(){ validateFileSize(this.id); });
 


function validateFileSize(id){
	
    if (window.File && window.FileReader && window.FileList && window.Blob)
    {
        //get the file size and file type from file input field
        var fsize = $("#"+id)[0].files[0].size;        
        if(fsize>1048576) //do something if file size more than 1 mb (1048576)
		{
			alert("El archivo debe ser menor a 1 mb.");
			var control = $("#"+id);
			control.replaceWith( control = control.clone( true ) );
			return false;
		}


	}
}



/////////////////////////////////////////////////////////Funciones generales
function formatear(id) {
    var valueString = $("#" + id).val().trim();
    var valueNumber = valueString.replace(/,/g, "");
    var valueFormat = addCommas(valueNumber);
    //console.log("recibi: "+valueString);
    //console.log("procese: "+valueNumber);
    //console.log("entrego: "+valueFormat);
    $("#" + id).val(valueFormat);
    //$("#help_"+id).html("$ "+addCommas(valueString));
}

function showDialog() {
    $("#load").fadeIn("fast");
}

function hideDialog() {
    $("#load").fadeOut("slow");
}
//Formateo de montos a (999,999,999)
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


function restringirNumeros(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

//Función para validar un RFC
// Devuelve el RFC sin espacios ni guiones si es correcto
// Devuelve false si es inválido
// (debe estar en mayúsculas, guiones y espacios intermedios opcionales)
function rfcValido(rfc, aceptarGenerico = true) {
    const re = /^([A-ZÑ&]{4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01]))?([A-Z\d]{3})$/;
    var validado = rfc.match(re);
    //console.log("FUnciont: "+validado);
    if (!validado) //Coincide con el formato general del regex?
        return false;

    return true;
}


//Handler para el evento cuando cambia el input
// -Lleva la RFC a mayúsculas para validarlo
// -Elimina los espacios que pueda tener antes o después
