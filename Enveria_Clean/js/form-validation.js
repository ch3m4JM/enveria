    $(function() {
      // Initialize form validation on the registration form.          
      var max3 = {
            required: true,
            digits: true,
            minlength: 3
      };
      var telephone = {
            required: true,
            digits: true,
            minlength: 10
      };
      var messmax3 = {
            required: "Campo obligatorio",
            digits: "Formato inválido",
            minlength: "El campo debe contener 3 digitos"
      };
      var messtelephone = {
            required: "Campo obligatorio",
            digits: "Formato inválido",
            minlength: "El teléfono debe contener 10 digitos"
      };
      var ruleSet1 = {
            required: true,
            digits: true
      };
      var ruleSet2 = {
            required: true,
            ruleSet2check:true
      };
      var messSet1 = {
            required: "Campo obligatorio",
            digits: "Formato inválido"
      };
      var messSet2 = {
            required: "Campo obligatorio",
            ruleSet2check: "Formato inválido"
      };

    $("form[id='form_test']").validate({
    
        rules: {
        //---------->validation_sesion--------------->//    
        login_pass: {
            required: true,
            pwcheck: true,
            minlength: 6
        },    
        email: {
            required: true,
            email: true
        },
        //---------->validation_registro--------------->//
        firstname: ruleSet2,
        lastname: ruleSet2,
        email1: {
            required: true,
            email: true
        },
        email2:{
            required: true,
            equalTo:"#email1"
        },
        password1: {
            required: true,
            pwcheck: true,
            minlength: 6
        },
        password2: {
            required: true,
            equalTo:"#password1"
        },
        //---------->validation_formulario_registro1--------------->//
        nombre: ruleSet2,
        apellido_paterno: ruleSet2,
        apellido_materno: ruleSet2,
        pais: {
            required: true
        },
        RFC:{
            required: true,
            rfcheck: true
        },
        telefono_1: telephone,
        calle: {
            required: true
        },
        numero_exterior: ruleSet1,
        codigo_Postal: ruleSet1,
        colonia:{
            required: true
        },
        delegacion:{
            required: true
        },
        Nom_Referencia_1: {
            required: true,
            wordcheck:true
        },
        Tel_Referencia_1: telephone,
        Nom_Referencia_2: {
            required: true,
            wordcheck:true
        },
        Tel_Referencia_2: telephone,

        //---------->validation_formulario_registro3--------------->//
        //Monto_Deseado: ruleSet1,
        Nombre_titular: ruleSet2,
        Numero_cuenta: ruleSet1,
        clabe: {
                required: true,
                digits: true,
                minlength:18
                }

    },
    // Specify validation error messages
    messages: {
        //---------->validation_registro--------------->//
        firstname: messSet2,
        lastname: messSet2,
        login_pass: {
        required: "Campo obligatorio",
        pwcheck: "Contraseña debe contener como mínimo una mayúscula y un número",
        minlength: "Contraseña debe ser mayor a 6 letras o números"
        },
        email1: {
            required: "Campo obligatorio",
            email: "Ingrese un correo válido",
        },
        email2: {
            required: "Campo obligatorio",
            equalTo: "Correos no coinciden"
        },
        password1: {
        required: "Campo obligatorio",
        pwcheck: "Contraseña debe contener como mínimo una mayúscula y un número",
        minlength: "Contraseña debe ser mayor a 6 letras o números"
        },
        password2: {
            required: "Campo obligatorio",
            equalTo: "Contraseñas no coinciden"
        },
        //---------->validation_formulario_registro1--------------->//
        email: "Ingrese un correo válido",
        nombre: messSet2,
        apellido_paterno: messSet2,
        apellido_materno: messSet2,
        pais: "Campo obligatorio",
        RFC: {
            required: "Campo obligatorio",
            rfcheck: "RFC formato incorrecto"
            },
        telefono_1: messtelephone,
        calle: "Campo obligatorio",
        numero_exterior: messSet1,
        codigo_Postal: messSet1,
        colonia:"Campo obligatorio",
        delegacion: "Campo obligatorio",
        Nom_Referencia_1: {
            required: "Campo obligatorio",
            wordcheck: "Nombre incompleto"
            }, 
        Tel_Referencia_1: messtelephone,
        Nom_Referencia_2: {
            required: "Campo obligatorio",
            wordcheck: "Nombre incompleto"
            }, 
        Tel_Referencia_2: messtelephone,
        //---------->validation_formulario_registro2--------------->//
        Empresa: messSet2,
        Puesto: messSet2,
        Ingreso_mensual: messSet2,
        Telefono_trabajo: messSet2,
        Promedio_Gasto: messSet2,
        Pago_Tarjetas: messSet2,
        Luz_Agua_Gas: messSet2,
        Entretenimiento: messSet2,
        Gastos_Transporte: messSet2,
        //---------->validation_formulario_registro3--------------->//
        //Monto_Deseado: messSet1,
        Nombre_titular: messSet2,
        Numero_cuenta: messSet1,
        clabe: messSet1
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
    
    $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
        && /[A-Z]/.test(value) // has an uppercase letter
        && /\d/.test(value) // has a digit
    });
    $.validator.addMethod("wordcheck", function(value) {
        return /^[a-zA-Z]* [a-zA-Z]+$/.test(value);
    });
    $.validator.addMethod("rfcheck", function(value) {
        return /^[A-Za-z0-9]*$/.test(value) // consists of only these
    });
    
    $.validator.addMethod("ruleSet2check", function(value) {
        return /^[a-zA-Z\s]+$/.test(value) // consists of only these
    });    
   
    $(function(){
        $('input[type="submit"]').click(function(){
        if ($('input[id="no_trabajo"]').is(':checked'))
        {
            var dialog = new BootstrapDialog({
                    title:'Información',
                    message: 'Por el momento no puedes continuar con tu solicitud, para poder solicitar un crédito de nómina es necesario que labores actualmente',
                    buttons: [{
                            label: 'OK',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                });
                dialog.open();
        }
        }); 
    });

});