<?php
include('./httpful.phar');


if(isset($_POST['submitDatos'])){
    session_start();
    ///----------------Fetch Token---------------///
    $accessToken = $_SESSION['acc_token'];
    $email = $_SESSION['email'];
    ///-----Save completeRegister Variables-----///
    $_SESSION['name'] = $_POST['nombre'];
    $_SESSION['firstLastName'] = $_POST['apellido_paterno'];
    $_SESSION['secondLastName'] = $_POST['apellido_materno'];
    $string= $_POST['fecha_nacimiento'];
    //change birthday format to ddmmyyy
    $birthDay = preg_replace('/\-/', '', $string);
    $_SESSION['birthDay']=$birthDay;
    $_SESSION['rfc']= $_POST['RFC'];
    $_SESSION['country']=484;
    $_SESSION['street']= $_POST['calle'];
    $_SESSION['number']= $_POST['numero_exterior'];
    $_SESSION['apartment']= $_POST['numero_interior'];
    $_SESSION['zipCode']=$_POST['codigo_Postal'];
    $_SESSION['suburb']=$_POST['colonia'];
    $_SESSION['municipality']=$_POST['delegacion'];
    $_SESSION['city']=$_POST['Ciudad'];
    $_SESSION['addressReferences']=$_POST['Referencia_ubicacion'];
    $_SESSION['Nom_Referencia_1']=$_POST['Nom_Referencia_1'];
    $_SESSION['Tel_Referencia_1']=$_POST['Tel_Referencia_1'];
    $_SESSION['Nom_Referencia_2']=$_POST['Nom_Referencia_2'];
    $_SESSION['Tel_Referencia_2']=$_POST['Tel_Referencia_2'];
    ///-----Request completeRegister Service-----///
    $name = $_SESSION['name'];
    $firstLastName = $_SESSION['firstLastName'];
    $secondLastName = $_SESSION['secondLastName'];
    $birthDay = $_SESSION['birthDay'];
    $rfc= $_SESSION['rfc'];
    $country=$_SESSION['country'];
    $street= $_SESSION['street'];
    $number= $_SESSION['number'];
    $apartment= $_SESSION['apartment'];
    $zipCode= $_SESSION['zipCode'];
    $suburb= $_SESSION['suburb'];
    $municipality= $_SESSION['municipality'];
    $city=  $_SESSION['city'];
    $addressReferences= $_SESSION['addressReferences'];
    $Nom_Referencia_1=$_SESSION['Nom_Referencia_1'];
    $Tel_Referencia_1=$_SESSION['Tel_Referencia_1'];
    $Nom_Referencia_2=$_SESSION['Nom_Referencia_2'];
    $Tel_Referencia_2=$_SESSION['Tel_Referencia_2'];

    $responsePersonal = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/completeRegister')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    ->body('{
    	        "clientID": "44b85676ff380ecee4380919388bb02099562e",
                "clientSecret": "49b5d420ccc8d8f49240b114a7febe052bf14b6",
                "accessToken": "'.$accessToken.'",
                "email":"'.$email.'",
                "firstName":"'.$name.'",
                "middleName": "Javier",
                "firstLastName": "'.$firstLastName.'",
                "secondLastName": "'.$secondLastName.'",
                "birthDay": "'.$birthDay.'",
                "rfc": "'.$rfc.'",
                "country": 52,
                "address": {
                    "street": "'.$street.'",
                    "number": "'.$number.'",
                    "apartment": "'.$apartment.'",
                    "zipCode": "'.$zipCode.'",
                    "suburb": "'.$suburb.'",
                    "municipality": "'.$municipality.'",
                    "city": "'.$city.'",
                    "state": "CDMX",
                    "addressReferences": "'.$addressReferences.'"	    
                },
                "contact": {
                    "house": "+525532323232",
                    "mobile": "+525554382902"
                },		
                "references": [
                    {
                        "name": "'.$Nom_Referencia_1.'",
                        "number": "'.$Tel_Referencia_1.'"			
                    },
                    {
                        "name": "'.$Nom_Referencia_2.'",
                        "number": "'.$Tel_Referencia_2.'"			
                    }	
                ]
	           
    }')// attach a body/payload...
    ->send();
    $dataPersonal = json_decode($responsePersonal, true);  
    echo "Complete Register Service: $responsePersonal";
    
    if (in_array('OK', $dataPersonal)) {
      
            header("Location:formulario_registro_5.html");
            exit();
    } 

    else{
        foreach($dataPersonal as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
        header("Refresh:0; url=formulario_registro_1.html");
        exit();
      
    };  
}
    
?>
