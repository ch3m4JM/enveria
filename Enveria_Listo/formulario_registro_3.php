<?php session_start(); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
<meta http equiv="X-UA-Compatible" content="IE=7">
<meta name="title" content="enver&a">
<meta name="keywords" content="Enveria, Prestamos.">
<meta name="description" content="Prestamos de efectivo.">
<meta  name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
<link rel="icon"  href="imagenes/favicon.png" />
<meta name="CATEGORY" content="home page"/>
<meta name="Expires" content="never"/>
<meta name="language" content="sp"/>
<meta name="VW96.objecttype" content="Document"/>
<meta name="resource-type" content="document"/>
<meta name="classification" content="health"/>
<meta name="Revisit" content="1 days"/>
<meta name="revisit-after" content="1 days"/>
<meta name="googlebot" content="default, follow, archive"/>
<meta name="audience" content="all"/>
<meta name="robots" content="ALL"/>
<meta name="distribution" content="Global"/>
<meta name="rating" content="General"/>
<meta name="copyright" content="(c) www.enveria.com.mx"/>
<meta name="doc-type" content="Public"/>
<meta name="doc-class" content="Completed"/>
<meta name="doc-rights" content="enveria.com.mx"/>
<meta name="doc-publisher" content="enveria"/>
<title>enver&amp;a</title>
</head>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script><!--Iframe-->  
<script type="text/javascript" src="js/jquery.responsiveiframe.js"></script><!--Iframe--> 

<script type='text/javascript'><!--Iframe--> 
  ir = responsiveIframe();
  ir.allowResponsiveEmbedding();  
</script>

<script type="text/javascript" src="js/jsDatePick.min.1.3.js"></script><!--Fecha-->

<script type="text/javascript"><!--Fecha-->
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%d-%M-%Y"
		});
		
		new JsDatePick({
			useMode:2,
			target:"inputFieldu",
			dateFormat:"%d-%M-%Y"
		});
	};
</script>


<link rel="stylesheet" type="text/css" media="all" href="css/jsDatePick_ltr.min.css" /><!--Fecha-->
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/><!--Fecha y Hora Nacional-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><!----Style de text de Errores---->
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<link rel="stylesheet" href="css/formulario.css" />
<link rel="stylesheet" href="css/formulario_registro.css" />

<style>
#boton_1 {background: url(imagenes/boton_1.png) no-repeat right;width:80px;}
#boton_1:hover { background: url(imagenes/boton_2.png) no-repeat right;}
 
#boton_2 {background: url(imagenes/boton_3.png) no-repeat right;width:80px;}
#boton_2:hover {background: url(imagenes/boton_4.png) no-repeat right;}
 
#boton_3 {background: url(imagenes/boton_5.png) no-repeat right;width:80px;}
#boton_3:hover {background: url(imagenes/boton_6.png) no-repeat right;}
 
#boton_4 {background: url(imagenes/boton_8.png) no-repeat right;width:80px;}
#boton_4:hover {background: url(imagenes/boton_8.png) no-repeat right;}
 
#boton_5 {background: url(imagenes/boton_9.png) no-repeat right;width:80px;}
#boton_5:hover {background: url(imagenes/boton_10.png) no-repeat right;}

#boton_6 {background: url(imagenes/boton_11.png) no-repeat right;width:80px;}
#boton_6:hover {background: url(imagenes/boton_12.png) no-repeat right;}

</style>
 
<body>
<?php

include('./httpful.phar');
$maxAmountToLend=$_SESSION['maxAmountToLend'];
    
if(isset($_POST['modal_trigger'])){

    $registerPrestamo = \Httpful\Request::post('http://10.0.10.180/New_API_JWT/API/Propuesta')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    ->body('{
	"idPropuesta": 0, 
     "Id": 3, 
     "intPeriodicidad": 2, 
     "intMoneda": 1, 
     "decCapital": 1000, 
     "decTasaIVA": 16, 
     "decMtoAnt": 0, 
     "strFechIni": "2017-11-15", 
     "strFechPrimerPago": "2017-11-30", 
     "intExigibilidad": 1, 
     "intCalendario": 1, 
     "intPlazo": 12, 
     "intEsqPago": 3, 
     "intTasa": 1, 
     "intTipoCalc": 1, 
     "decTasaBase": 0, 
     "decPuntos": 18, 
     "decFactor": 0, 
     "decTasaNominal": 18, 
     "decMtoValRes": 0, 
     "intFormaPago": 3, 
     "intClaveMora": 1, 
     "intTipoCalculoMora": 11, 
     "decBaseMora": 0, 
     "decPuntosMora": 36, 
     "decFactorMora": 0, 
     "decTasaNominalmora": 36, 
     "intPCvePaquete": 0, 
     "intPCveOpcion": 0, 
     "strCveTOper": "CS", 
     "intCveEmpr": 1
    }')// attach a body/payload...
    ->send(); 
    $dataLoan = json_decode($registerPrestamo, true);  
	$output = "";

    if (in_array('OK', $dataLoan)) {
    echo $dataLoan;   
    } 

    else{
        foreach($dataLoan as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };    
}

?>

<section id="portada1"> 

<article id="menu">


<section id="broche">
<!--<a href="cuenta.html"><article class="cheta"><div id="boton_1"><img  class="usua" src="imagenes/Trans.png"></div></article></a>-->
<a href="formulario_registro_2.php"><article class="cheta"><div id="boton_3"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_1.php"><article class="cheta"><div id="boton_2"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_5.php"><article class="cheta"><div id="boton_6"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_3.php"><article class="cheta"><div id="boton_4"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_4.php"><article class="cheta"><div id="boton_5"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
</section>


</article>                    

<article id="formulario">
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script><!--Validacion de campos-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script><!--Validacion de campos-->
<script  src="js/form-validation.js"></script> <!--Validacion de campos--> 
<!--DIVISION-->
<section id="rosas">
<form action="formulario_registro_3.php" method="post" id="form_test">

<article class="ross">
<table border="0">
    <tr><td><siux>Solicitud de crédito</siux> <h4>Campos obligatorios*</h4></td></tr>
    </tr>
</table>
</article>


<article class="ross">
</article>

<article class="ross">
<table border="0">
<tr><td colspan="4"width="200%"><h2>Datos del crédito</h2><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="Monto_Deseado" title="Monto_Deseado" value= "<?php echo $maxAmountToLend; ?>" placeholder="Monto Deseado*" size=25><br><br></td></tr>
<tr>
<td colspan="4"width="200%">
<select style="height: 40px; width: 98%;" class="salto" name="Plazo" title="Plazo">
  				<option value="2_Quincenas">12 Meses</option>
</select><br><br>
</td>
</tr>

<tr>
	  <td colspan="4"width="200%">
        <div id="azul">
          <a type="submit" name="modal_trigger" id="modal_trigger" href="#modal" class="btn_red">Solicita tu crédito</a>
          
          <input name="modal_trigger" type=submit value="Guardar y continuar" class="btn_red">
        </div>
      </td>
</tr>

<tr>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
</table>

</article>



<!--
<article class="ross"> 5 </article>
<article class="ross"> 6 </article>
-->
</form>
</section>

</article>
                        
</section>
<script>/*ToolTips*/
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
</body>
</html>
