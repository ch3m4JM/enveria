$(function(){
        $('input[type="submit"]').click(function(){
        $("form[id='form_auth']").validate({
    
        rules: {
        pin: {
            required: true
        },
        passauth: {
            required: true,
            equalTo: "#buroauth"
            }

        },
        // Specify validation error messages
        messages: {
        //---------->validation_authentication_buro--------------->//
        pin: {
            required: "Campo obligatorio"
        },
        passauth: {
            required: "Campo obligatorio",
            equalTo: "Contraseña incorrecta"
        }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
        form.submit();
        }
        });

        }); 
});
