$(window).load(function () {

    //$("#Ingreso_mensual").mask("999?,999,999", { placeholder : "" });
    //$("#load").removeClass("loader");
    //$("#bodyTable").html("");//Limpiar tabla
    $("#load").fadeOut("slow");

});


//////////////////////////////////////////////Al presionar el boton
$("#calcular_credito").click(function () {
    //calcularCredito(2000,3,2);

    //Se obtienen los valores establecidos
    var montoString = $("#Monto_Deseado").val();
    var monto = montoString.replace(/,/g, "");
    var quincenas = $("#Plazo").val();
    var tipoCredito = $("#Destino_credito").val();

    //console.log("Motno: " + monto);
    //console.log("Plazo: " + quincenas);
    //console.log("Tipo: " + tipoCredito);

    //Se valida que esten todos los valores establecidos
    if (monto == "" || quincenas == null || tipoCredito == null) {
        if (monto == "") {
            alert("Debe establecer un monto");
        } else if (quincenas == null) {
            alert("Debe establecer un plazo");
        } else if (tipoCredito == null) {
            alert("Debe establecer un destino para el crédito");
        } else {
            alert("Debe ingresar todos los campos solicitados");
        }
    }
    //Se envia la petición
    else {
        //calcularCredito(monto, quincenas, tipoCredito);
        getJSON(monto, quincenas, tipoCredito);
    }

});

/////////////////////////////////////////////Enviar petición

function getJSON(monto, quincenas, tipoCredito) {
    showDialog(); //Se muestra el loadinf

    //armar json de peticion
    var jsonRequest = {
        clientID: '44b85676ff380ecee4380919388bb02099562e',
        clientSecret: '49b5d420ccc8d8f49240b114a7febe052bf14b6',
        reqAmount: monto,
        creditType: tipoCredito,
        term: quincenas
    };

    //Se convierte al formato json
    var peticion = JSON.stringify(jsonRequest);
    var jss = JSON.parse(peticion);
    //console.log(jss);

    //Se envia la petición asincrona con ajax
    $.ajax({
        url: 'simulador.php', //php que trae hace la peticion a java
        headers: {
            'Content-Type': 'application/json'
        },
        contentType: 'application/json; charset=utf-8',
        //type: 'POST',
        dataType: 'JSON',
        method: 'POST',
        data: {
            request: peticion
        },
        //Cuando php responde con exito
        success: function (data) {
            //Se obtiene la respuesta y convierte a JSON
            //var obj = Object.values(data);

            var obj = Object.keys(data).map(function (e) {
                return data[e]
            });
            
            var jsonString = JSON.stringify(obj);
            var jsonRequest = JSON.parse(jsonString);
            //console.log(jsonRequest);

            //Se obtiene el object JSON que contiene los resultados
            var table = JSON.parse(jsonRequest[1]);
            var filas = table.amortizationTable;
            //console.log(filas);

            //Se limpia la tabla
            var cuerpoTabla = $("#bodyTable");
            cuerpoTabla.html("");

            //Se colocan los resultados en la tabla
            $.each(filas, function (i, item) {
                //console.log(item);
                //console.log(item.totalBalance);

                var numeroPago = item.paymentNumber;
                var cuotaApertura = item.openingPayment;
                var interes = item.toInterest;
                var capital = item.toCapital;
                var total = item.totalPayment;
                var saldo = item.totalBalance;

                var cuerpo = "<tr bgcolor='#E4E2E2' align='center'>                                                                                <td width='1%'><xy>" + numeroPago + "</xy></td>                                                                      <td width='1%'><xy>$" + cuotaApertura + "</xy></td>                                                                  <td width='1%'><xy>$" + interes + "</xy></td>                                                                        <td width='1%'><xy>$" + capital + "</xy></td>                                                                        <td width='1%'><xy>$" + total + "</xy></td>                                                                          <td width='1%'><xy>$" + saldo + "</xy></td>                                                                         </tr>";


                cuerpoTabla.append(cuerpo); //Se añade cada fila
            });

            hideDialog(); //Se oculta el loading
        }
    });
}

function calcularCredito(monto, plazo, tipoCredito) {
    showDialog();

    //armar json de peticion
    var jsonRequest = {
        clientID: '44b85676ff380ecee4380919388bb02099562e',
        clientSecret: '49b5d420ccc8d8f49240b114a7febe052bf14b6',
        reqAmount: 2000,
        creditType: 1,
        term: 12
    };

    var js = JSON.stringify(jsonRequest);
    var jss = JSON.parse(js);

    console.log(jss);

    $.ajax({
        url: 'http://200.52.77.113:8080/EnveriaBackOffice/quickCreditSimulation',
        /*headers: {
            'Content-Type' :'application/json'
        },*/
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        dataType: 'JSON',
        //method: 'POST',
        data: jsonRequest,
        success: function (data) {
            //var d= JSON.parse(data);
            console.log(Object.entries(data));
            console.log(Object.values(data));
            var d = Object.entries(data).toString();

            console.log("data: " + d);
            console.log("dataJ: " + j);

        }
    });
}

////Hacer restricciones
$("#Monto_Deseado").keydown(function (e) {
    var id = $(this).attr("id");
    restringirNumeros(e);
});
$("#Monto_Deseado").keyup(function (e) {
    var id = $(this).attr("id");
    formatear(id);
});


//////////////////////////////////////////////Funciones Generales
function showDialog() {
    $("#load").fadeIn("fast");
}

function hideDialog() {
    $("#load").fadeOut("slow");
}

function formatear(id) {
    var valueString = $("#" + id).val().trim();
    var valueNumber = valueString.replace(/,/g, "");
    var valueFormat = addCommas(valueNumber);
    //console.log("recibi: "+valueString);
    //console.log("procese: "+valueNumber);
    //console.log("entrego: "+valueFormat);
    $("#" + id).val(valueFormat);
    //$("#help_"+id).html("$ "+addCommas(valueString));
}

//Formateo de montos a (999,999,999)
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


function restringirNumeros(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
