$(function() {
      // Initialize form validation on the registration form.
        /*$.validator.methods.digits = function ( value, element ) {

            return this .optional( element ) || /[az] +@ [az]+\.[az]+/ .test( value );
            }*/
        
      // It has the name attribute "registration"   
        $('input[type="submit"]').click(function(){
        if ($('input[id="no_trabajo"]').is(':checked'))
        {
            var dialog = new BootstrapDialog({
                    title:'Información',
                    message: 'Por el momento no puedes continuar con tu solicitud, para poder solicitar un crédito de nómina es necesario que labores actualmente',
                    buttons: [{
                            label: 'OK',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                });
                dialog.open();
        }
        }); 
      var phoneWork = {
            required: true,
            digits: true,
            minlength:10
      };
      var messPhoneWork = {
            required: "Campo obligatorio",
            digits: "Formato inválido",
            minlength: "El teléfono debe contener 10 dígitos"
      };
      var ruleSet1 = {
            required: true,
            digits: true
      };
      var ruleSet2 = {
            required: true,
            lettersonly: true
      };
      var messSet1 = {
            required: "Campo obligatorio",
            digits: "Formato inválido"
      };
      var messSet2 = {
            required: "Campo obligatorio",
            lettersonly: "Formato inválido"
      };


      $("form[id='form_work']").validate({
      rules:{
           //---------->validation_formulario_registro2--------------->//
        Empresa: ruleSet2,
        Puesto: ruleSet2,
        //Ingreso_mensual: ruleSet1,
        Telefono_trabajo: phoneWork,
        //Promedio_Gasto: ruleSet1,
        //Pago_Tarjetas: ruleSet1,
        //Luz_Agua_Gas: ruleSet1,
        //Entretenimiento: ruleSet1,
        //Gastos_Transporte: ruleSet1,
        //Otros_Gastos:
        Destino_credito: {required: true},
        Pep : {required: true}
      },
       
    // Specify validation error messages
    messages: {
        //---------->validation_formulario_registro2--------------->//
        Empresa: messSet2,
        Puesto: messSet2,
        Ingreso_mensual: messSet1,
        Telefono_trabajo: messPhoneWork,
        Promedio_Gasto: messSet1,
        Pago_Tarjetas: messSet1,
        Luz_Agua_Gas: messSet1,
        Entretenimiento: messSet1,
        Gastos_Transporte: messSet1,
        Otros_Gastos: messSet1,
        Destino_credito:"Campo obligatorio",
        Pep: "Seleccione una de las opciones"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
    

});



