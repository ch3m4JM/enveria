<?php
include('./httpful.phar');

//Se valida que sea una peticion json
if(isset($_GET['request'])){
    //Se obtiene el Json de petición
    $peticion=$_GET['request'];
    
    //Se obtiene la peticion
    $company = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/quickCreditSimulation')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    /*->body('
        {
        "clientID" : "44b85676ff380ecee4380919388bb02099562e",
        "clientSecret" : "49b5d420ccc8d8f49240b114a7febe052bf14b6",
        "reqAmount":2000,
        "creditType":2,
        "term":3
    }')// attach a body/payload...*/
    ->body($peticion)// attach a body/payload...
    ->send(); 
    $companies = json_decode($company, true);  
    
    //Se regresa a ajax el json de respuesta
    echo json_encode($company,false);
}else{
     $error ="{'error': 'Petición invalida'}";
    //echo json_encode($error,false);
    echo $_POST['postData'];
}
?>
