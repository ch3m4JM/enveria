
<?php

include('./httpful.phar');

if(isset($_POST['submitDatos'])){
    ///----------------Fetch Token---------------///
    $accessToken = $_SESSION['acc_token'];
    
    ///-----Request completeRegister Service-----///
    
    $name = $_POST['nombre'];
    $firstLastName = $_POST['apellido_paterno'];
    $secondLastName = $_POST['apellido_materno'];
    $email = $_POST['email'];
    $string= $_POST['fecha_nacimiento'];
    //change birthday format to ddmmyyy
    $birthDay = preg_replace('/\-/', '', $string);
    $rfc= $_POST['RFC'];
    $country=484;
    $street= $_POST['calle'];
    $number= $_POST['numero_exterior'];
    $apartment= $_POST['numero_interior'];
    $zipCode= $_POST['codigo_Postal'];
    $suburb= $_POST['colonia'];
    $municipality= $_POST['delegacion'];
    $city= $_POST['Ciudad'];
    $addressReferences= $_POST['Referencia_ubicacion'];
    $Nom_Referencia_1=$_POST['Nom_Referencia_1'];
    $Tel_Referencia_1=$_POST['Tel_Referencia_1'];
    $Nom_Referencia_2=$_POST['Nom_Referencia_2'];
    $Tel_Referencia_2=$_POST['Tel_Referencia_2'];

    $responsePersonal = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/completeRegister')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    ->body('{
    	        "clientID": "44b85676ff380ecee4380919388bb02099562e",
                "clientSecret": "49b5d420ccc8d8f49240b114a7febe052bf14b6",
                "accessToken": "'.$accessToken.'",
                "email":"'.$email.'",
                "firstName":"'.$name.'",
                "middleName": "Javier",
                "firstLastName": "'.$firstLastName.'",
                "secondLastName": "'.$secondLastName.'",
                "birthDay": "'.$birthDay.'",
                "rfc": "'.$rfc.'",
                "country": 52,
                "address": {
                    "street": "'.$street.'",
                    "number": "'.$number.'",
                    "apartment": "'.$apartment.'",
                    "zipCode": "'.$zipCode.'",
                    "suburb": "'.$suburb.'",
                    "municipality": "'.$municipality.'",
                    "city": "'.$city.'",
                    "state": "CDMX",
                    "addressReferences": "'.$addressReferences.'"	    
                },
                "contact": {
                    "house": "+525532323232",
                    "mobile": "+525554382902"
                },		
                "references": [
                    {
                        "name": "'.$Nom_Referencia_1.'",
                        "number": "'.$Tel_Referencia_1.'"			
                    },
                    {
                        "name": "'.$Nom_Referencia_2.'",
                        "number": "'.$Tel_Referencia_2.'"			
                    }	
                ]
	           
    }')// attach a body/payload...
    ->send();
    $dataPersonal = json_decode($responsePersonal, true);  
    echo "Complete Register Service: $responsePersonal";
    
    if (in_array('OK', $dataPersonal)) {
        ///-----Request incomeExpenses Service-----///
            //$email = $_SESSION['email'];
        echo "Registro Completo";
        $employeeId = $_SESSION['employeeId'];
        $position = $_SESSION['position'];
        $jobSince = $_SESSION['jobSince'];
        $salary = $_SESSION['salary'];
        $phone = $_SESSION['phone'];
        $rent = $_SESSION['rent'];
        $creditCards = $_SESSION['creditCards'];
        $utilities = $_SESSION['utilities'];
        $entertainment = $_SESSION['entertainment'];
        $transportation = $_SESSION['transportation'];
        $otherExpenses = $_SESSION['otherExpenses'];

        $responseWork = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/incomeExpenses')// Build a PUT request...
        ->sendsJson() // tell it we're sending (Content-Type) JSON...
        ->body('{
                "clientID": "44b85676ff380ecee4380919388bb02099562e",
                "clientSecret": "49b5d420ccc8d8f49240b114a7febe052bf14b6",
                "accessToken": "'.$accessToken.'",
                "email":"'.$email.'",
                "position":"'.$position.'",
                "jobSince":"30/12/2016",
                "employeeId":"F123",
                "sector":"",
                "pep":"Y",
                "pepFamilyMember":"Esposa",
                "creditPurpose":"viaje",
                "jobAddress": {
                        "street": "Toltecas",
                        "number": "166",
                        "apartment": "J404",
                        "zipCode": "01180",
                        "suburb": "Carola",
                        "municipality": "Alvaro Obregon",
                        "city": "CDMX",
                        "state": "CDMX",
                        "addressReferences": "Entre toltecas y ferrocarril de cuernavaca",
                        "phone":"+521111111111",
                        "ext":"4221"
                    },		
                "income":{
                    "salary":"'.$salary.'",
                    "otherIncome":"2000"	
                    },	
                "monthlyExpenses":{
                    "rent":"'.$rent.'",
                    "creditCards":"'.$creditCards.'",
                    "departamentalCards":"4000",
                    "mortgage":"0",
                    "utilities":"'.$utilities.'",
                    "entertainment":"'.$entertainment.'",
                    "transportation":"'.$transportation.'",
                    "otherExpenses":"'.$otherExpenses.'"	
                   }		
                }       
        ')// attach a body/payload...
        ->send();
        $dataPrestamo = json_decode($responseWork, true); 
        echo "Income Service: $responseWork";   
    
        
        if (in_array('OK', $dataPrestamo)) {

         $_SESSION['maxAmountToLend']=$dataPrestamo['maxAmountToLend'];
         echo $_SESSION['maxAmountToLend'];
        }
        
        else{
        echo "Error income";
        foreach($dataPersonal as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };  
        
    } 

    else{
        foreach($dataPersonal as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };  
      
}
    
?>
