<?php session_start(); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
<meta http equiv="X-UA-Compatible" content="IE=7">
<meta name="title" content="enver&a">
<meta name="keywords" content="Enveria, Prestamos.">
<meta name="description" content="Prestamos de efectivo.">
<meta  name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
<link rel="icon"  href="imagenes/favicon.png" />
<meta name="CATEGORY" content="home page"/>
<meta name="Expires" content="never"/>
<meta name="language" content="sp"/>
<meta name="VW96.objecttype" content="Document"/>
<meta name="resource-type" content="document"/>
<meta name="classification" content="health"/>
<meta name="Revisit" content="1 days"/>
<meta name="revisit-after" content="1 days"/>
<meta name="googlebot" content="default, follow, archive"/>
<meta name="audience" content="all"/>
<meta name="robots" content="ALL"/>
<meta name="distribution" content="Global"/>
<meta name="rating" content="General"/>
<meta name="copyright" content="(c) www.enveria.com.mx"/>
<meta name="doc-type" content="Public"/>
<meta name="doc-class" content="Completed"/>
<meta name="doc-rights" content="enveria.com.mx"/>
<meta name="doc-publisher" content="enveria"/>
<title>enver&amp;a</title>
</head>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script><!--Iframe-->  
<script type="text/javascript" src="js/jquery.responsiveiframe.js"></script><!--Iframe--> 

<script type='text/javascript'><!--Iframe--> 
  ir = responsiveIframe();
  ir.allowResponsiveEmbedding();  
</script>


<script type="text/javascript"><!--Fecha-->
$(document).ready(function() {
    $('#datePicker')
        .datepicker({
            autoclose: 'true',
            format: 'mm-dd-yyyy'
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $(this).datepicker('hide');
            $('#eventForm').formValidation('revalidateField', 'date');
        });

    $('#eventForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            date: {
                validators: {
                    notEmpty: {
                        message: 'The date is required'
                    },
                    date: {
                        format: 'MM/DD/YYYY',
                        message: 'The date is not a valid'
                    }
                }
            }
        }
    });
});
</script>

<link rel="stylesheet" type="text/css" media="all" href="css/jsDatePick_ltr.min.css" /><!--Fecha-->
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/><!--Fecha y Hora Nacional-->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><!----Style de text de Errores---->
<link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
<link rel="stylesheet" href="css/formulario.css" />
<link rel="stylesheet" href="css/formulario_registro.css" />

<style>
#boton_1 {background: url(imagenes/boton_1.png) no-repeat right;width:80px;}
#boton_1:hover { background: url(imagenes/boton_2.png) no-repeat right;}
 
#boton_2 {background: url(imagenes/boton_4.png) no-repeat right;width:80px;}
#boton_2:hover {background: url(imagenes/boton_4.png) no-repeat right;}
 
#boton_3 {background: url(imagenes/boton_5.png) no-repeat right;width:80px;}
#boton_3:hover {background: url(imagenes/boton_6.png) no-repeat right;}
 
#boton_4 {background: url(imagenes/boton_7.png) no-repeat right;width:80px;}
#boton_4:hover {background: url(imagenes/boton_8.png) no-repeat right;}
 
#boton_5 {background: url(imagenes/boton_9.png) no-repeat right;width:80px;}
#boton_5:hover {background: url(imagenes/boton_10.png) no-repeat right;}

#boton_6 {background: url(imagenes/boton_11.png) no-repeat right;width:80px;}
#boton_6:hover {background: url(imagenes/boton_12.png) no-repeat right;}
    

</style>

<body>

<?php

include('./httpful.phar');

if(isset($_POST['submitDatos'])){
    ///----------------Fetch Token---------------///
    $accessToken = $_SESSION['acc_token'];
    
    ///-----Request completeRegister Service-----///
    
    $name = $_POST['nombre'];
    $firstLastName = $_POST['apellido_paterno'];
    $secondLastName = $_POST['apellido_materno'];
    $email = $_POST['email'];
    $string= $_POST['fecha_nacimiento'];
    //change birthday format to ddmmyyy
    $birthDay = preg_replace('/\-/', '', $string);
    $rfc= $_POST['RFC'];
    $country=484;
    $street= $_POST['calle'];
    $number= $_POST['numero_exterior'];
    $apartment= $_POST['numero_interior'];
    $zipCode= $_POST['codigo_Postal'];
    $suburb= $_POST['colonia'];
    $municipality= $_POST['delegacion'];
    $city= $_POST['Ciudad'];
    $addressReferences= $_POST['Referencia_ubicacion'];
    $Nom_Referencia_1=$_POST['Nom_Referencia_1'];
    $Tel_Referencia_1=$_POST['Tel_Referencia_1'];
    $Nom_Referencia_2=$_POST['Nom_Referencia_2'];
    $Tel_Referencia_2=$_POST['Tel_Referencia_2'];

    $responsePersonal = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/completeRegister')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    ->body('{
    	        "clientID": "44b85676ff380ecee4380919388bb02099562e",
                "clientSecret": "49b5d420ccc8d8f49240b114a7febe052bf14b6",
                "accessToken": "'.$accessToken.'",
                "email":"'.$email.'",
                "firstName":"'.$name.'",
                "middleName": "Javier",
                "firstLastName": "'.$firstLastName.'",
                "secondLastName": "'.$secondLastName.'",
                "birthDay": "'.$birthDay.'",
                "rfc": "'.$rfc.'",
                "country": 52,
                "address": {
                    "street": "'.$street.'",
                    "number": "'.$number.'",
                    "apartment": "'.$apartment.'",
                    "zipCode": "'.$zipCode.'",
                    "suburb": "'.$suburb.'",
                    "municipality": "'.$municipality.'",
                    "city": "'.$city.'",
                    "state": "CDMX",
                    "addressReferences": "'.$addressReferences.'"	    
                },
                "contact": {
                    "house": "+525532323232",
                    "mobile": "+525554382902"
                },		
                "references": [
                    {
                        "name": "'.$Nom_Referencia_1.'",
                        "number": "'.$Tel_Referencia_1.'"			
                    },
                    {
                        "name": "'.$Nom_Referencia_2.'",
                        "number": "'.$Tel_Referencia_2.'"			
                    }	
                ]
	           
    }')// attach a body/payload...
    ->send();
    $dataPersonal = json_decode($responsePersonal, true);  
    echo "Complete Register Service: $responsePersonal";
    
    if (in_array('OK', $dataPersonal)) {
        ///-----Request incomeExpenses Service-----///
            //$email = $_SESSION['email'];
        echo "Registro Completo";
        $employeeId = $_SESSION['employeeId'];
        $position = $_SESSION['position'];
        $jobSince = $_SESSION['jobSince'];
        $salary = $_SESSION['salary'];
        $phone = $_SESSION['phone'];
        $rent = $_SESSION['rent'];
        $creditCards = $_SESSION['creditCards'];
        $utilities = $_SESSION['utilities'];
        $entertainment = $_SESSION['entertainment'];
        $transportation = $_SESSION['transportation'];
        $otherExpenses = $_SESSION['otherExpenses'];

        $responseWork = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/incomeExpenses')// Build a PUT request...
        ->sendsJson() // tell it we're sending (Content-Type) JSON...
        ->body('{
                "clientID": "44b85676ff380ecee4380919388bb02099562e",
                "clientSecret": "49b5d420ccc8d8f49240b114a7febe052bf14b6",
                "accessToken": "'.$accessToken.'",
                "email":"'.$email.'",
                "position":"'.$position.'",
                "jobSince":"30/12/2016",
                "employeeId":"F123",
                "sector":"",
                "pep":"Y",
                "pepFamilyMember":"Esposa",
                "creditPurpose":"viaje",
                "jobAddress": {
                        "street": "Toltecas",
                        "number": "166",
                        "apartment": "J404",
                        "zipCode": "01180",
                        "suburb": "Carola",
                        "municipality": "Alvaro Obregon",
                        "city": "CDMX",
                        "state": "CDMX",
                        "addressReferences": "Entre toltecas y ferrocarril de cuernavaca",
                        "phone":"+521111111111",
                        "ext":"4221"
                    },		
                "income":{
                    "salary":"'.$salary.'",
                    "otherIncome":"2000"	
                    },	
                "monthlyExpenses":{
                    "rent":"'.$rent.'",
                    "creditCards":"'.$creditCards.'",
                    "departamentalCards":"4000",
                    "mortgage":"0",
                    "utilities":"'.$utilities.'",
                    "entertainment":"'.$entertainment.'",
                    "transportation":"'.$transportation.'",
                    "otherExpenses":"'.$otherExpenses.'"	
                   }		
                }       
        ')// attach a body/payload...
        ->send();
        $dataPrestamo = json_decode($responseWork, true); 
        echo "Income Service: $responseWork";   
    
        
        if (in_array('OK', $dataPrestamo)) {

         $_SESSION['maxAmountToLend']=$dataPrestamo['maxAmountToLend'];
         echo $_SESSION['maxAmountToLend'];
        }
        
        else{
        echo "Error income";
        foreach($dataPersonal as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };  
        
    } 

    else{
        foreach($dataPersonal as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };  
      
}
    
?>


<section id="portada1"> 

<article id="menu">


<section id="broche">
<!--<a href="cuenta.html"><article class="cheta"><div id="boton_1"><img  class="usua" src="imagenes/Trans.png"></div></article></a>-->
<a href="formulario_registro_2.php"><article class="cheta"><div id="boton_3"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_1.php"><article class="cheta"><div id="boton_2"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_5.php"><article class="cheta"><div id="boton_6"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_3.php"><article class="cheta"><div id="boton_4"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_4.php"><article class="cheta"><div id="boton_5"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
</section>


</article>                    

<article id="formulario"> 
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> <!--Validacion de campos-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script><!--Validacion de campos-->
<script  src="js/form-validation.js"></script> <!--Validacion de campos-->

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />                              <!--Fecha-->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />                             <!--Fecha-->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>                                             <!--Fecha-->


<!--DIVISION-->
<section id="rosas">
<form action="formulario_registro_3.php" method="post" id="form_test">

<article class="ross">
<table border="0">
    <tr><td><siux>Solicitud de crédito</siux> <h4>Campos obligatorios*</h4></td></tr>
    </tr>
</table>
</article>

<article class="ross">

</article>


<article class="ross">
<table border="0">
<tr><td colspan="4"width="200%"><h2>Datos personales</h2></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="nombre" title="nombre" required id="nombre" placeholder="Nombre(s)*" size=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="apellido_paterno" title="Apellido_Paterno" required id="Apellido Paterno" placeholder="Apellido Paterno*" size=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="apellido_materno" title="Apellido_Materno" required id="Apellido Materno" placeholder="Apellido Materno*" size=25><br><br></td></tr><tr><td colspan="4"width="200%"><input class="salto" type="text" name="fecha_nacimiento" title="fecha_nacimiento" size="12" id="datePicker" required id="Fecha de nacimiento" placeholder="Fecha de nacimiento*"/><br><br></td></tr> 
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="pais" title="Pais" required id="Pais" placeholder="Pais*" size=25><br><br></td></tr>   
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="RFC" title="RFC" required id="RFC" placeholder="RFC*" maxlenght=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="email" name="email" title="Email" required id="email" placeholder="Correo electrónico" size=25><br><br></td></tr> 
    <tr>
      <td><h2>Número de contacto</h2></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
</table>

<table border="0">
    <tr>
      <td width="7%"><input class="salto" type="tel" name="largaDistancia_1" title="LargaDistancia_1" placeholder="Larga Distancia" maxlength=3><br><br></td>
      <td width="7%"><input class="salto" type="tel" name="clave_1" title="Clave_1" required id="Clave Lada" placeholder="Clave*" maxlength=3><br><br></td>
      <td width="7%"><input class="salto" type="tel" name="telefono_1" title="Telefono_1" required id="Teléfono" placeholder="Teléfono*" maxlength=10><br><br></td>
      <td width="7%">&nbsp;<br><br></td>
    </tr>
</table>

</article>

<article class="ross">

<table border="0">
    <tr>
      <td><h2>Domicilio</h2></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr><td colspan="4"width="200%"><input class="salto" type="text" name="calle" title="calle" required id="calle" placeholder="Calle*" size=25><br><br></td></tr>
</table>

<table border="0">
    <tr>
      <td width="7%"><input class="salto" type="text" name="numero_exterior" title="Numero_exterior" placeholder="Número exterior" size=25><br><br></td>
      <td width="7%"><input class="salto" type="text" name="numero_interior" title="Numero_interior" required id="Número interior" placeholder="Número interior*" size=25><br><br></td>
      <td width="7%"><input class="salto" type="text" name="codigo_Postal" title="Codigo_Postal" required id="Código Postal" placeholder="Código Postal*" size=25><br><br></td>
      <td width="7%">&nbsp;</td>
    </tr>
</table>

<table border="0">
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="colonia" title="Colonia" required id="Colonia" placeholder="Colonia*" size=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="delegacion" title="Delegacion" required id="Delegacion" placeholder="Delegación o Mpio.*" size=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="Ciudad" title="Ciudad" placeholder="Ciudad" size=25><br><br></td></tr>
<tr><td colspan="4"width="200%"><input class="salto" type="text" name="Referencia_ubicacion" title="Referencia_ubicacion" size="12" id="inputField" required id="Referencia de Ubicación" placeholder="Referencia de Ubicación"/><br><br></td></tr>
<tr>
	<td><h2>Referencias personales (2)</h2></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
</table>

<table border="0">
    <tr>
      <td width="7%"><input class="salto" type="text" name="Nom_Referencia_1" title="Nom_Referencia_1" required id="Nombre de referencia 1" placeholder="Nombre*" size=25><br><br></td>
      <td width="7%"><input class="salto" type="tel" name="Tel_Referencia_1" title="Tel_Referencia_1" required id="Teléfono de referencia 1" placeholder="Teléfono*" maxlength=25><br><br></td>
    </tr>
    
    <tr>
      <td width="7%"><input class="salto" type="text" name="Nom_Referencia_2" title="Nom_Referencia_2" required id="Nombre de referencia 2" placeholder="Nombre*" size=25><br><br></td>
      <td width="7%"><input class="salto" type="tel" name="Tel_Referencia_2" title="Tel_Referencia_2" required id="Teléfono de referencia 2" placeholder="Teléfono*" maxlength=25><br><br></td>
    </tr>
    
    <tr>
	  <td colspan="4"width="200%">
        <div id="azul">
          <input name="submitDatos" type=submit value="Guardar y continuar" class="button"><br><br>
        </div>
      </td>
    </tr>
</table>


</article>


<!--
<article class="ross"> 5 </article>
<article class="ross"> 6 </article>
-->
</form>
</section>

</article>
                        
</section>
  


</body>
</html>
