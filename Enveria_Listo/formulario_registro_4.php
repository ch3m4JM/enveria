<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
<meta http equiv="X-UA-Compatible" content="IE=7">
<meta name="title" content="enver&a">
<meta name="keywords" content="Enveria, Prestamos.">
<meta name="description" content="Prestamos de efectivo.">
<meta  name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
<link rel="icon"  href="imagenes/favicon.png" />
<meta name="CATEGORY" content="home page"/>
<meta name="Expires" content="never"/>
<meta name="language" content="sp"/>
<meta name="VW96.objecttype" content="Document"/>
<meta name="resource-type" content="document"/>
<meta name="classification" content="health"/>
<meta name="Revisit" content="1 days"/>
<meta name="revisit-after" content="1 days"/>
<meta name="googlebot" content="default, follow, archive"/>
<meta name="audience" content="all"/>
<meta name="robots" content="ALL"/>
<meta name="distribution" content="Global"/>
<meta name="rating" content="General"/>
<meta name="copyright" content="(c) www.enveria.com.mx"/>
<meta name="doc-type" content="Public"/>
<meta name="doc-class" content="Completed"/>
<meta name="doc-rights" content="enveria.com.mx"/>
<meta name="doc-publisher" content="enveria"/>
<title>enver&amp;a</title>
</head>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script><!--Iframe-->  
<script type="text/javascript" src="js/jquery.responsiveiframe.js"></script><!--Iframe--> 

<script type='text/javascript'><!--Iframe--> 
  ir = responsiveIframe();
  ir.allowResponsiveEmbedding();  
</script>

<link rel="stylesheet" href="css/formulario.css" />
<link rel="stylesheet" href="css/formulario_registro.css" />

<style>
#boton_1 {background: url(imagenes/boton_1.png) no-repeat right;width:80px;}
#boton_1:hover { background: url(imagenes/boton_2.png) no-repeat right;}
 
#boton_2 {background: url(imagenes/boton_3.png) no-repeat right;width:80px;}
#boton_2:hover {background: url(imagenes/boton_4.png) no-repeat right;}
 
#boton_3 {background: url(imagenes/boton_5.png) no-repeat right;width:80px;}
#boton_3:hover {background: url(imagenes/boton_6.png) no-repeat right;}
 
#boton_4 {background: url(imagenes/boton_7.png) no-repeat right;width:80px;}
#boton_4:hover {background: url(imagenes/boton_8.png) no-repeat right;}
 
#boton_5 {background: url(imagenes/boton_10.png) no-repeat right;width:80px;}
#boton_5:hover {background: url(imagenes/boton_10.png) no-repeat right;}

#boton_6 {background: url(imagenes/boton_11.png) no-repeat right;width:80px;}
#boton_6:hover {background: url(imagenes/boton_12.png) no-repeat right;}
</style>
<body>

<?php

include('./httpful.phar');
    
if(isset($_POST['login1'])){

    $uploadFile = \Httpful\Request::post('https://127.0.0.1:8080/EnveriaBackOffice/UploadServlet')// Build a PUT request...
    ->sendsJson() // tell it we're sending (Content-Type) JSON...
    ->body('{
   
    }')// attach a body/payload...
    ->send(); 
    $upload = json_decode($register, true);  
	$output = "";
    
    if (in_array('OK', $upload)) {
        
    } 

    else{
        
        foreach($upload as $value){ 
              $error = $value['description'];
        }
        echo '<script type="text/javascript">
        alert("'.$error.'");</script>';
      
    };    
}

?>


<section id="portada1"> 

<article id="menu">


<section id="broche">
<!--<a href="cuenta.html"><article class="cheta"><div id="boton_1"><img  class="usua" src="imagenes/Trans.png"></div></article></a>-->
<a href="formulario_registro_2.php"><article class="cheta"><div id="boton_3"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_1.php"><article class="cheta"><div id="boton_2"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_5.php"><article class="cheta"><div id="boton_6"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_3.php"><article class="cheta"><div id="boton_4"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
<a href="formulario_registro_4.php"><article class="cheta"><div id="boton_5"><img  class="usua" src="imagenes/Trans.png"></div></article></a>
</section>


</article>                 

<article id="formulario"> 
<!--DIVISION-->
<section id="rosas">
<form action="envia.php" method="post" id="form_test">
<article class="ross">

<table border="0">
    <tr><td><siux>Solicitud de crédito</siux> <h4>Por favor adjunta la siguiente documentación*</h4></td></tr>
    </tr>
</table>

</article>


<article class="ross">
</article>

<article class="ross">

<table border="0" width="100%">
    <tr>
      <td >
      <h1>1&nbsp;&bull;&nbsp;Identificación oficial vigente* (IFE, Pasaporte ó Cedula profesional)</h1>
      </td>
    </tr>
    <tr>
      <td >
       <input name="identificacion_oficial" type="file" size="20"> <br><br>
      </td>
    </tr>
    
    <tr>
      <td  >
      <h1>2&nbsp;&bull;&nbsp;Comprobante de domicilio no mayor a tres meses* (Agua, Luz, Teléfono)</h1>
      </td>
    </tr>
    <tr>
      <td >
      <input name="comprobante_domicilio" type="file" size="20"> <br><br>
      </td>
    </tr>
</table>
</center>
</article>

<article class="ross">
<center>
<table border="0" width="100%">
    <tr>
      <td >
      <h1>3&nbsp;&bull;&nbsp;Estado de cuenta no mayor a tres meses*</h1>
      </td>
    </tr>
    <tr  >
      <td >
      <input name="estado_cuenta" type="file" size="20"> <br><br>
      </td>
    </tr>
    
    <tr >
      <td >
      <h1>4&nbsp;&bull;&nbsp;Últimos 6 recibos de nómina*</h1>
      </td>
    </tr>
    <tr >
      <td >
      <input name="recibos_nomina" type="file" size="20"> <br><br>
      </td>
    </tr>
    
    <tr>
	  <td colspan="4"width="200%">
        <div id="azul">
          <input name="submit" type=submit value="Finalizar solicitud" class="button"><br><br>
        </div>
      </td>
</tr>
</table>

</article>

<!--<article class="ross"> 5 </article>

<article class="ross"> 6 </article>-->

</form>
</section>

</article>
                        
</section>
  


</body>
</html>
